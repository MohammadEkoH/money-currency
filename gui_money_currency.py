# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'money_currency.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def Generate(self, Pilih_negara = 0) :  
        # Variable
        file = 0.0

        dictionary={}
        val={}
        
        nn = 0
        mod = {}
        mod[0] = 0
        mod[1] = 0
        nY = {}
        nY[0] = 0.0
        nY[1] = 0.0
        now = {}
        now[0] = 0.0
        now[1] = 0.0
        xx = {}
        xx[0] = 0.0
        xx[1] = 0.0

        X = {}
        X[0] = 0.0
        X[1] = 0.0
        x = {}
        x[0] = 0.0
        x[1] = 0.0
        y = {}
        y[0] = 0.0
        y[1] = 0.0
        Y = {}
        Y[0] = 0.0
        Y[1] = 0.0
        XY = {}
        XY[0] = 0.0
        XY[1] = 0.0
        X2 = {}
        X2[0] = 0.0
        X2[1] = 0.0
        totalX = {}
        totalX[0] = 0.0
        totalX[1] = 0.0
        totalY = {}
        totalY[0] = 0.0
        totalY[1] = 0.0
        totalXY = {}
        totalXY[0] = 0.0
        totalXY[1] = 0.0
        totalX2 = {}
        totalX2[0] = 0
        totalX2[1] = 0
        A = {}
        A[0] = 0.0
        A[1] = 0.0
        B = {}
        B[0] = 0.0
        B[1] = 0.0
        C = {} 
        C[0] = 0.0
        C[1] = 0.0
        
        x[0] = list() 
        x[1] = list() 
        y[0] = list() 
        y[1] = list() 

        if Pilih_negara == 1 : 
            file = open('data_trend/USD.txt').readlines()  
        elif Pilih_negara == 2 :
            file = open('data_trend/AUD.txt').readlines()  
        elif Pilih_negara == 3 :
            file = open('data_trend/JPY.txt').readlines()  
        elif Pilih_negara == 4 :
            file = open('data_trend/CNH.txt').readlines()  
        elif Pilih_negara == 5 :
            file = open('data_trend/THB.txt').readlines()  
        else :
            print "Pilih_negara 1-5"   

        n = 0

        for value in file: 
            val = value.split(' ')
            dictionary[val[0]] = val[0]
            dictionary[val[1]] = val[1] 
            
            Y[0] = float(dictionary[val[0]])
            Y[1] = float(dictionary[val[1]])  

            totalY[0] += Y[0] 
            totalY[1] += Y[1]    
            
            y[0].append(Y[0])
            y[1].append(Y[1])  

            # self.table_data.setItem(0, n, QtGui.QTableWidgetItem(nY[0]))
            # self.setHorizontalHeaderLabels(horHeaders)
            
            nY[0] = nY[0] + 1
            nY[1] = nY[1] + 1   

        mod[0] = int(nY[0]%2) 
        mod[1] = int(nY[1]%2)   
        if mod[0] == 0 & mod[1] == 0 :
            # print "genap"
            
            nY[0] = int(nY[0])
            nY[1] = int(nY[1]) 
            xx[0] = nY[0] + 1
            xx[1] = nY[1] + 1  
            now[0] = int((nY[0]/2) - nY[0])   
            now[1] = int((nY[1]/2) - nY[1])   
            now[0] += now[0]
            now[1] += now[1] 
            for i in range(0, nY[0]):   
                kondisi = now[0] % 2 
                if kondisi == 0 :
                    now[0] += 1
                    now[1] += 1
                    X[0] = now[0]
                    X[1] = now[1]
                    
                    x[0].append(X[0]) 
                    x[1].append(X[1]) 

                    totalX[0] += X[0]
                    totalX[1] += X[1]

                    X2[0] = (X[1] ** 2)
                    X2[1] = (X[0] ** 2)
                    
                    totalX2[0] = totalX2[0] + X2[0]
                    totalX2[1] = totalX2[1] + X2[1]
            
                now[0] += 1  
                now[1] += 1   
        else :
            # print "ganjil"

            n = 0 
            nY[0] = int(nY[0])   
            nY[1] = int(nY[1])   
            now[0] = int((nY[0]/2) - nY[0])
            now[1] = int((nY[1]/2) - nY[1])   
            now[0] += 1 
            now[1] += 1 
            xx[0] = nY[0] / 2+1 
            xx[1] = nY[1] / 2+1 
            for i in range(0, nY[0]):  
                X[0] = now[0]
                X[1] = now[1]
                
                x[0].append(X[0]) 
                x[1].append(X[1]) 

                totalX[0] += X[0]
                totalX[1] += X[1]

                X2[0] = (X[0] ** 2)
                X2[1] = (X[1] ** 2)
                
                totalX2[0] += X2[0]
                totalX2[1] += X2[1] 
                
                now[0] += 1
                now[1] += 1  

        # print x[0]
        # print y[0]
        # print x[1]
        # print y[1]

        nn = 0
        n = 0
        if len(x[0]) == len(y[0]):
            n = int(nY[0] + nY[1])
            while nn < nY[0] :   
                XY[0] = x[0][nn] * y[0][nn]
                XY[1] = x[1][nn] * y[1][nn] 

                totalXY[0] += XY[0]
                totalXY[1] += XY[1] 
 
                nn += 1

        elif len(x)!= len(y):
            print('Tidak terdifinisi') 

        # print totalX2
        # print totalXY

        A[0] = totalY[0] / nY[0]
        A[1] = totalY[1] / nY[1] 

        B[0] = float(totalXY[0] / totalX2[0])
        B[1] = float(totalXY[1] / totalX2[1])    

        # print(str("{:.2f}".format(A[0])) + " + " + str("{:.2f}".format(B[0])) + " * " + str(xx[0]))
        # print(str("{:.2f}".format(A[1])) + " + " + str("{:.2f}".format(B[1])) + " * " + str(xx[1])) 
 
        C[0] = A[0] + (B[0] * (xx[0])) 
        C[1] = A[1] + (B[1] * (xx[1]))  
 
        mod = nY[0] % 2
        if mod == 0 :
            xx[0] = xx[0]
            xx[1] = xx[1]
        else : 
            xx[0] = nY[0]+1
            xx[1] = nY[1]+1

        # print("Forecast nilai jual hari ke-" + str(xx[0]) + " : " + str("{:.2f}".format(C[0]))) 
        # print("Forecast nilai beli hari ke-" + str(xx[1]) + " : " + str("{:.2f}".format(C[1])))  

        if Pilih_negara == 1 : 
            with open('data_trend/USD.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 2 :
            with open('data_trend/AUD.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 3 :
            with open('data_trend/JPY.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0]))) 
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 4 :
            with open('data_trend/CNH.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))  
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 5 :
            with open('data_trend/THB.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))  
                save.write(' ' + str("{:.2f}".format(C[1])))  

        if Pilih_negara == 1:
            Pilih_negara = "USD"
        if Pilih_negara == 2:
            Pilih_negara = "AUD"
        if Pilih_negara == 3:
            Pilih_negara = "JPY"
        if Pilih_negara == 4:
            Pilih_negara = "CNH"
        if Pilih_negara == 5:
            Pilih_negara = "THB"    

        self.list_hasil.addItem(Pilih_negara + " hari ke-" + str(xx[0]) + " - Jual: " +  str("{:.2f}".format(C[0])) + ", Beli: " + str("{:.2f}".format(C[1]))) 
        # self.list_hasil.addItem("nilai beli " + Pilih_negara + " hari ke-" + str(xx[1]) + " : " + str("{:.2f}".format(C[1])))

    def hitung(self, i) :
        self.list_hasil.clear()
        # Eksekutor
        prediksi = 0  
        pilih = 0 
 
        prediksi = int(self.spin_prediksi.text())   
        
        if prediksi > 0:
            
            if i == 1 :
                print "USD"
                pilih = 1
            elif i == 2 :
                print "AUD"
                pilih = 2
            elif i == 3 :
                print "JPY"
                pilih = 3
            elif i == 4 :
                print "CNH"
                pilih = 4
            elif i == 5 :
                print "THB"
                pilih = 5  
        else : 
            print "gagal"

         
        for ulangi in range(0, int(prediksi)) : 
            self.Generate(pilih)  

    def clear(self) :
        self.list_hasil.clear()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(443, 517)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label_hari = QtWidgets.QLabel(self.centralwidget)
        self.label_hari.setGeometry(QtCore.QRect(20, 70, 131, 17))
        self.label_hari.setObjectName("label_hari")
        self.btn_hitung = QtWidgets.QPushButton(self.centralwidget)
        self.btn_hitung.setGeometry(QtCore.QRect(20, 320, 401, 31))
        self.btn_hitung.setObjectName("btn_hitung")
        self.label_by = QtWidgets.QLabel(self.centralwidget)
        self.label_by.setGeometry(QtCore.QRect(130, 460, 191, 20))
        self.label_by.setObjectName("label_by")
        self.spin_prediksi = QtWidgets.QSpinBox(self.centralwidget)
        self.spin_prediksi.setGeometry(QtCore.QRect(20, 90, 71, 31))
        self.spin_prediksi.setObjectName("spin_prediksi")
        self.pilih_money = QtWidgets.QComboBox(self.centralwidget)
        self.pilih_money.setGeometry(QtCore.QRect(100, 90, 91, 31))
        self.pilih_money.setEditable(False)
        self.pilih_money.setObjectName("pilih_money") 
        self.pilih_money.addItems(["-Pilih-","USD", "AUD", "JPY", "CNH", "THB"]) 
        self.pilih_money.currentIndexChanged.connect(self.hitung)
        self.label_hasil = QtWidgets.QLabel(self.centralwidget)
        self.label_hasil.setGeometry(QtCore.QRect(20, 150, 81, 17))
        self.label_hasil.setObjectName("label_hasil")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(360, 90, 61, 31))
        self.pushButton.setObjectName("pushButton")
        self.list_hasil = QtWidgets.QListWidget(self.centralwidget)
        self.list_hasil.setGeometry(QtCore.QRect(20, 170, 401, 141))
        self.list_hasil.setObjectName("list_hasil")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_hari.setText(_translate("MainWindow", "Input Prediksi :"))
        self.btn_hitung.setText(_translate("MainWindow", "Hitung"))
        self.label_by.setText(_translate("MainWindow", "BY : Mohammad Eko Hardiyanto"))
        self.label_hasil.setText(_translate("MainWindow", "Hasil"))
        self.pushButton.setText(_translate("MainWindow", "Clear"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

