class Trend_perbarui : 
    def LimaHariKedepan(Pilih_negara = 0) : 

        # Variable
        file = 0.0

        dictionary={}
        val={}
        
        nn = 0
        mod = {}
        mod[0] = 0
        mod[1] = 0
        nY = {}
        nY[0] = 0.0
        nY[1] = 0.0
        now = {}
        now[0] = 0.0
        now[1] = 0.0
        xx = {}
        xx[0] = 0.0
        xx[1] = 0.0

        X = {}
        X[0] = 0.0
        X[1] = 0.0
        x = {}
        x[0] = 0.0
        x[1] = 0.0
        y = {}
        y[0] = 0.0
        y[1] = 0.0
        Y = {}
        Y[0] = 0.0
        Y[1] = 0.0
        XY = {}
        XY[0] = 0.0
        XY[1] = 0.0
        X2 = {}
        X2[0] = 0.0
        X2[1] = 0.0
        totalX = {}
        totalX[0] = 0.0
        totalX[1] = 0.0
        totalY = {}
        totalY[0] = 0.0
        totalY[1] = 0.0
        totalXY = {}
        totalXY[0] = 0.0
        totalXY[1] = 0.0
        totalX2 = {}
        totalX2[0] = 0
        totalX2[1] = 0
        A = {}
        A[0] = 0.0
        A[1] = 0.0
        B = {}
        B[0] = 0.0
        B[1] = 0.0
        C = {} 
        C[0] = 0.0
        C[1] = 0.0
        
        x[0] = list() 
        x[1] = list() 
        y[0] = list() 
        y[1] = list() 

        if Pilih_negara == 1 : 
            file = open('data_trend/USD.txt').readlines()  
        elif Pilih_negara == 2 :
            file = open('data_trend/AUD.txt').readlines()  
        elif Pilih_negara == 3 :
            file = open('data_trend/JPY.txt').readlines()  
        elif Pilih_negara == 4 :
            file = open('data_trend/CNH.txt').readlines()  
        elif Pilih_negara == 5 :
            file = open('data_trend/THB.txt').readlines()  
        else :
            print "Pilih_negara 1-5"   

        for value in file: 
            val = value.split(' ')
            dictionary[val[0]] = val[0]
            dictionary[val[1]] = val[1] 
            
            Y[0] = float(dictionary[val[0]])
            Y[1] = float(dictionary[val[1]]) 

            totalY[0] += Y[0] 
            totalY[1] += Y[1]    
            
            y[0].append(Y[0])
            y[1].append(Y[1])  
     
            nY[0] = nY[0] + 1
            nY[1] = nY[1] + 1      

        mod[0] = int(nY[0]%2) 
        mod[1] = int(nY[1]%2)   
        if mod[0] == 0 & mod[1] == 0 :
            # print "genap"
            
            nY[0] = int(nY[0])
            nY[1] = int(nY[1]) 
            xx[0] = nY[0] + 1
            xx[1] = nY[1] + 1  
            now[0] = int((nY[0]/2) - nY[0])   
            now[1] = int((nY[1]/2) - nY[1])   
            now[0] += now[0]
            now[1] += now[1] 
            for i in range(0, nY[0]):   
                kondisi = now[0] % 2 
                if kondisi == 0 :
                    now[0] += 1
                    now[1] += 1
                    X[0] = now[0]
                    X[1] = now[1]
                    
                    x[0].append(X[0]) 
                    x[1].append(X[1]) 

                    totalX[0] += X[0]
                    totalX[1] += X[1]

                    X2[0] = (X[1] ** 2)
                    X2[1] = (X[0] ** 2)
                    
                    totalX2[0] = totalX2[0] + X2[0]
                    totalX2[1] = totalX2[1] + X2[1]
            
                now[0] += 1  
                now[1] += 1   
        else :
            # print "ganjil"

            n = 0 
            nY[0] = int(nY[0])   
            nY[1] = int(nY[1])   
            now[0] = int((nY[0]/2) - nY[0])
            now[1] = int((nY[1]/2) - nY[1])   
            now[0] += 1 
            now[1] += 1 
            xx[0] = nY[0] / 2+1 
            xx[1] = nY[1] / 2+1 
            for i in range(0, nY[0]):  
                X[0] = now[0]
                X[1] = now[1]
                
                x[0].append(X[0]) 
                x[1].append(X[1]) 

                totalX[0] += X[0]
                totalX[1] += X[1]

                X2[0] = (X[0] ** 2)
                X2[1] = (X[1] ** 2)
                
                totalX2[0] += X2[0]
                totalX2[1] += X2[1] 
                
                now[0] += 1
                now[1] += 1  

        print x[0]
        print y[0]
        print x[1]
        print y[1]

        nn = 0
        n = 0
        if len(x[0]) == len(y[0]):
            n = int(nY[0] + nY[1])
            while nn < nY[0] :   
                XY[0] = x[0][nn] * y[0][nn]
                XY[1] = x[1][nn] * y[1][nn] 

                totalXY[0] += XY[0]
                totalXY[1] += XY[1] 
 
                nn += 1

        elif len(x)!= len(y):
            print('Tidak terdifinisi') 

        # print totalX2
        # print totalXY

        A[0] = totalY[0] / nY[0]
        A[1] = totalY[1] / nY[1] 

        B[0] = float(totalXY[0] / totalX2[0])
        B[1] = float(totalXY[1] / totalX2[1])    

        print(str("{:.2f}".format(A[0])) + " + " + str("{:.2f}".format(B[0])) + " * " + str(xx[0]))
        print(str("{:.2f}".format(A[1])) + " + " + str("{:.2f}".format(B[1])) + " * " + str(xx[1])) 
 
        C[0] = A[0] + (B[0] * (xx[0])) 
        C[1] = A[1] + (B[1] * (xx[1]))  
 
        mod = nY[0] % 2
        if mod == 0 :
            xx[0] = xx[0]
            xx[1] = xx[1]
        else : 
            xx[0] = nY[0]+1
            xx[1] = nY[1]+1

        print("Forecast nilai jual hari ke-" + str(xx[0]) + " : " + str("{:.2f}".format(C[0]))) 
        print("Forecast nilai beli hari ke-" + str(xx[1]) + " : " + str("{:.2f}".format(C[1]))) 

        if Pilih_negara == 1 : 
            with open('data_trend/USD.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 2 :
            with open('data_trend/AUD.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 3 :
            with open('data_trend/JPY.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0]))) 
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 4 :
            with open('data_trend/CNH.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))  
                save.write(' ' + str("{:.2f}".format(C[1])))
        elif Pilih_negara == 5 :
            with open('data_trend/THB.txt', 'a') as save:
                save.write('\n' + str("{:.2f}".format(C[0])))  
                save.write(' ' + str("{:.2f}".format(C[1])))

    # Eksekutor
    ulangi = 0  
    print "1. USD \n2. AUD \n3. JPY \n4. CNH \n5. THB" 
    input_negara = int(raw_input("Pilih_negara (1 - 5) : "))
    input = int(raw_input("Prediksi berapa hari kedepan : "))

    for ulangi in range(0, input) :
        LimaHariKedepan(input_negara)